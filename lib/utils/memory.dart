import 'dart:convert';

import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/poll.dart';
import 'package:codework_itas/models/user.dart';
import 'package:codework_itas/utils/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Memory {
  SharedPreferences _preferences;
  Future<List<PollModel>> get polls async {
    this._preferences = await SharedPreferences.getInstance();
    dynamic pollsJson = await this._preferences.getString("polls");
    List<PollModel> polls = [];
    if (pollsJson != null) {
      Map<String, dynamic> pollDecoded = jsonDecode(pollsJson);
      (pollDecoded['polls']).forEach((poll) {
        var pollModel = new PollModel();
        pollModel.setAsks(poll['asks']);
        pollModel.setAskTitle(poll['ask_title']);
        pollModel.setType(poll['type']);
        polls.add(pollModel);
      });
    }
    return polls;
  }

  Future<void> addPoll(PollModel poll) async {
    this._preferences = await SharedPreferences.getInstance();
    dynamic pollsJson = await this._preferences.getString("polls");
    if (pollsJson != null) {
      Map<String, dynamic> pollDecoded = jsonDecode(pollsJson);
      pollDecoded['polls'].add(poll.toJson());
      await this._preferences.setString("polls", jsonEncode(pollDecoded));
    } else {
      var encoded = jsonEncode({
        "polls": [poll.toJson()]
      });
      await this._preferences.setString("polls", encoded);
    }
  }

  void initPollMock() {
    var polls = [
      new PollModel()
          .setAsks(["มีการดำเนินการ", "ไม่มีการดำเนินการ"])
          .setAskTitle("หน่วยงานมีการกำหนดกลไกลหรือไม่")
          .setType(TYPES[0]),
      new PollModel()
          .setAsks(["มีการดำเนินการ", "ไม่มีการดำเนินการ"])
          .setAskTitle("หน่วยงานมีการเปิดเผยข้อมูลหรือไม่")
          .setType(TYPES[1]),
      new PollModel()
          .setAsks(["มีการดำเนินการ", "ไม่มีการดำเนินการ"])
          .setAskTitle("หน่วยงานมีการกำหนดกลไกลหรือไม่")
          .setType(TYPES[0]),
      new PollModel()
          .setAsks(["มีการดำเนินการ", "ไม่มีการดำเนินการ"])
          .setAskTitle("หน่วยงานมีการกำหนดกลไกลหรือไม่")
          .setType(TYPES[0]),
      new PollModel()
          .setAsks(["มีการดำเนินการ", "ไม่มีการดำเนินการ"])
          .setAskTitle("หน่วยงานมีการกำหนดการป้องกันหรือไม่")
          .setType(TYPES[1]),
    ];
    polls.forEach((poll) async {
      await this.addPoll(poll);
    });
  }

  Future<UserModel> getUserInCache() async {
    this._preferences = await SharedPreferences.getInstance();
    var userEncoded = await this._preferences.getString("user");
    if (userEncoded != null) {
      Map<String, dynamic> userDecoded = json.decode(userEncoded);
      return UserModel.fromJson(userDecoded);
    }
  }

  Future<bool> login(String username, String password) async {
    this._preferences = await SharedPreferences.getInstance();
    var userEncoded = await this._preferences.getString("user");
    Map<String, dynamic> userDecoded = json.decode(userEncoded);
    if (username == userDecoded['username'] &&
        password == userDecoded['password']) {
      return true;
    }
    return false;
  }

  Future<void> channgeInfoUser(UserModel user) async {
    this._preferences = await SharedPreferences.getInstance();
    await this._preferences.setString("user", jsonEncode(user.toJson()));
  }

  Future<bool> changePassword(old, password) async {
    this._preferences = await SharedPreferences.getInstance();
    var userEncoded = await this._preferences.getString("user");
    Map<String, dynamic> userDecoded = json.decode(userEncoded);
    var user = UserModel.fromJson(userDecoded);

    if (user.password != old) {
      return false;
    }

    user.setPassword(password);

    User.user = user;

    await this.channgeInfoUser(User.user);

    return true;
  }

  void initialUser() async {
    this._preferences = await SharedPreferences.getInstance();
    var userEncoded = await this._preferences.getString("user");
    if (userEncoded == null) {
      UserModel user = new UserModel();
      user
          .setAffiliation("")
          .setDepartment("")
          .setEmail("")
          .setFirstname("นายอมร")
          .setLastname("ศรีชัย")
          .setPassword("1234")
          .setUsername("test01")
          .setGroup("")
          .setType("");
      this._preferences.setString("user", jsonEncode(user));
    }
  }
}
