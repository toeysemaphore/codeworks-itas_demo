import 'package:codework_itas/models/poll.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class BaseColor {
  static Color IMPERIAL = Color(0xFF643A71);
  static Color MIDDLE_PURPLE = Color(0xFF8B5FBF);
  static Color PASTEL_VIOLET = Color(0xFFD183C9);
  static Color RUDDY_PINK = Color(0xFFE3879E);
  static Color BUMBLE_GUM = Color(0xFFFEC0CE);
}

class TriggerPoll {
  static PublishSubject<PollModel> pollT = PublishSubject();
}

final theme = ThemeData(
  fontFamily: "prompt",
  scaffoldBackgroundColor: Colors.white,
  appBarTheme: AppBarTheme(elevation: 20, color: BaseColor.MIDDLE_PURPLE),
  textTheme: TextTheme(
    headline: TextStyle(
      color: Colors.white,
      fontSize: 25,
      letterSpacing: 2,
      fontWeight: FontWeight.bold,
    ),
    title: TextStyle(
      color: Colors.white,
      fontSize: 23,
      letterSpacing: 1.3,
    ),
    subtitle: TextStyle(
      color: Colors.white,
      fontSize: 20,
      letterSpacing: 1.3,
    ),
    display1: TextStyle(
      color: Colors.white,
      fontSize: 15,
    ),
    display2: TextStyle(
      color: Colors.white,
      fontSize: 18,
    ),
    button: TextStyle(
      color: Colors.white,
      fontSize: 30,
      letterSpacing: 2,
    ),
  ),
);

final TYPES = [
  "เลือกได้มากกว่าหนึ่ง",
  "เลือกอย่างใดอย่างหนึ่ง",
];

List<String> AFFILLATIONS() => [
      "กระทรวงคมนาคม",
      "กระทรวงวัฒนธรรม",
      "กระทรวงกลาโหม",
      "กระทรวงการคลัง",
      "กระทรวงการต่างประเทศ"
    ];

List<String> YEARS_BUDGET() =>
    List.generate(11, (i) => (2562 - i).toString()).toList();

List<String> DEPARTMENTS() => [
      "กรมเจ้าท่า",
      "กรมขนส่งทางบก",
    ];
