import 'package:codework_itas/const.dart';
import 'package:codework_itas/pages/common/go_to/screen.dart';
import 'package:codework_itas/pages/explore_eit/screen.dart';
import 'package:codework_itas/pages/explore_followup/screen.dart';
import 'package:codework_itas/pages/explore_iit/screen.dart';
import 'package:codework_itas/pages/explore_oit/screen.dart';
import 'package:codework_itas/pages/login/screen.dart';
import 'package:codework_itas/pages/main/screen.dart';
import 'package:codework_itas/pages/me/screen.dart';
import 'package:codework_itas/pages/notification/screen.dart';
import 'package:codework_itas/pages/poll/screen.dart';
import 'package:codework_itas/pages/reporting/subpage/eit.dart';
import 'package:codework_itas/pages/reporting/subpage/evaluated_sum.dart';
import 'package:codework_itas/pages/reporting/subpage/history.dart';
import 'package:codework_itas/pages/reporting/subpage/iit.dart';
import 'package:codework_itas/pages/reporting/subpage/oit.dart';
import 'package:codework_itas/pages/reporting/subpage/sumary.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:codework_itas/widgets/poll.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    Memory().initialUser();
    Memory().initPollMock();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "itas",
      debugShowCheckedModeBanner: false,
      theme: theme,
      routes: {
        "/": (BuildContext context) => !true ? GoToPage() : LoginPage(),
        "/main": (BuildContext context) => MainPage(),
        "/eit": (BuildContext context) => EitPage(),
        "/goto": (BuildContext context) => GoToPage(),
        "/oit": (BuildContext context) => OitPage(),
        "/iit": (BuildContext context) => IitPage(),
        "/followup": (BuildContext context) => FollowUpPage(),
        "/notification": (BuildContext context) => NotificationPage(),
        "/reporting/eit": (BuildContext context) => ReportEitSubpage(),
        "/reporting/oit": (BuildContext context) => ReportOitSubpage(),
        "/reporting/iit": (BuildContext context) => ReportIitSubpage(),
        "/reporting/summary": (BuildContext context) =>
            ReportingSummarySubpage(),
        "/reporting/evaluate": (BuildContext context) => ReportingResult(),
        "/reporting/history": (BuildContext context) => ReportingHistoryPage(),
        "/me": (BuildContext context) => MEPage(),
        "/poll": (BuildContext context) => PollPage(),
        "/poll/maker": (BuildContext context) => PollMaker(),
      },
    );
  }
}
