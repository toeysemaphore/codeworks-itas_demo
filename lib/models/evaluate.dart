import 'package:flutter/material.dart';

class EvaluateModel {
  String level;
  int numberOfDepartment;
  double percentage;
  Color color;

  EvaluateModel(
      this.level, this.numberOfDepartment, this.percentage, this.color);

  Map<String, dynamic> toJsonTH() => {
        "ระดับ": this.level,
        "จำนวนหน่วยงาน": this.numberOfDepartment,
        "จำนวนร้อยละ": this.percentage
      };
}
