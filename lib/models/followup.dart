class FollowupModel {
  String affilation;
  String department;
  String oit;
  int iit;
  int eit;

  FollowupModel({
    this.eit,
    this.iit,
    this.oit,
    this.department,
    this.affilation,
  });

  Map<String, String> toJsonTH() => {
        "สังกัด": this.affilation,
        "หน่วยงาน": this.department,
        "แบบสำรวจ OIT": this.oit,
        "แบบสำรวจ IIT": this.iit.toString(),
        "แบบสำรวจ EIT": this.eit.toString(),
      };
}
