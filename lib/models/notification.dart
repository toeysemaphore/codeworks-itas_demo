class NotificationModel {
  final String title;
  final String body;
  bool _isNew;
  NotificationModel(this.title, this.body, [this._isNew = false]);
  void setNew() => this._isNew = true;
  bool get isNew => this._isNew;
}
