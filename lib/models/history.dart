class HistoryModel {
  String date;
  String ip;
  String username;
  String department;
  String menu;
  String desc;

  HistoryModel({
    this.date,
    this.department,
    this.desc,
    this.ip,
    this.menu,
    this.username,
  });

  Map<String, String> toJson() => {
        "date": this.date,
        "department": this.department,
        "desc": this.desc,
        "ip": this.ip,
        "menu": this.menu,
        "username": this.username,
      };

  Map<String, String> toJsonTH() => {
        "วันเวลา": this.date,
        "ip": this.ip,
        "username": this.username,
        "หน่วยงาน": this.department,
        "เมนู": this.menu,
        "คำอธิบาย": this.desc,
      };
}
