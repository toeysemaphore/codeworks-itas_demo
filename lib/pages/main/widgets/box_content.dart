import 'package:flutter/material.dart';

class BoxContentWidget extends StatelessWidget {
  final double width;
  final double height;
  final String title;
  final Widget child;

  BoxContentWidget(
    this.height,
    this.width,
    this.title,
    this.child,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      height: this.height,
      child: Stack(
        children: <Widget>[
          Positioned(
            top: 15,
            child: Container(
              height: this.height - 14,
              width: this.width,
              decoration: BoxDecoration(
                border: Border.all(
                  width: 4,
                  color: Colors.black,
                ),
              ),
            ),
          ),
          Positioned(
            left: 20,
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(color: Colors.black),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: this.child,
          )
        ],
      ),
    );
  }
}
