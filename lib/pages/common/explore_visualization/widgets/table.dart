import 'package:codework_itas/const.dart';
import 'package:flutter/material.dart';
import 'package:json_table/json_table.dart';

class TableWidget extends StatelessWidget {
  List<Map<String, dynamic>> json;
  bool _seq;
  TableWidget(this.json, [this._seq = false]);

  @override
  Widget build(BuildContext context) {
    if (this._seq) {
      this.json = this
          .json
          .asMap()
          .map(
            (i, j) => MapEntry(
              i + 1,
              {
                "ลำดับที่": (i + 1).toString(),
                ...j,
              },
            ),
          )
          .values
          .toList();
      print("H");
    }

    return Container(
      child: JsonTable(
        json,
        tableHeaderBuilder: (String header) {
          Color colorExplore;
          if (header == "สำรวจแล้ว" || header == "ยังไม่สำรวจ")
            colorExplore = header == "สำรวจแล้ว"
                ? BaseColor.MIDDLE_PURPLE
                : BaseColor.PASTEL_VIOLET;
          else
            colorExplore = Colors.black;

          return Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            decoration: BoxDecoration(
                border: Border.all(width: 0.5, color: BaseColor.IMPERIAL),
                color: colorExplore),
            child: Text(
              header,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1.copyWith(
                  fontWeight: FontWeight.w700,
                  fontSize: 14.0,
                  color: Colors.white),
            ),
          );
        },
        tableCellBuilder: (value) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
            decoration: BoxDecoration(
                border: Border.all(width: 0.5, color: BaseColor.IMPERIAL)),
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .display1
                  .copyWith(fontSize: 14.0, color: Colors.grey[900]),
            ),
          );
        },
      ),
    );
  }
}
