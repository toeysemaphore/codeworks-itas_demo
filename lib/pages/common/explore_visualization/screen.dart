import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/visual_explore.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/bar.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

class ExploreVisualization extends StatefulWidget {
  List<Map<String, dynamic>> _data;
  List<VisualExplore> _mock = [];
  String _title;
  ExploreVisualization(
      [this._title = "รายงาน",this._data = const [
        {"type": "test1", "สำรวจแล้ว": 10, "ยังไม่สำรวจ": 20},
        {"type": "test2", "สำรวจแล้ว": 20, "ยังไม่สำรวจ": 20},
        {"type": "test3", "สำรวจแล้ว": 15, "ยังไม่สำรวจ": 30},
        {
          "type": "teasdasdasasdd;ksals;ddasdsst4",
          "สำรวจแล้ว": 50,
          "ยังไม่สำรวจ": 30
        },
        {"type": "test5", "สำรวจแล้ว": 100, "ยังไม่สำรวจ": 20},
        {"type": "test6", "สำรวจแล้ว": 65, "ยังไม่สำรวจ": 70},
        {"type": "test7", "สำรวจแล้ว": 32, "ยังไม่สำรวจ": 34},
        {"type": "test8", "สำรวจแล้ว": 23, "ยังไม่สำรวจ": 30},
        {"type": "test9", "สำรวจแล้ว": 50, "ยังไม่สำรวจ": 80},
      ]]) {
        int index = 0;
    this._data = this._data.map((d) {
      index++;
      this
          ._mock
          .add(new VisualExplore(d['type'], d['สำรวจแล้ว'], d['ยังไม่สำรวจ']));
      return {
        "ลำดับ": index.toString(),
        "type": d['type'],
        "สำรวจแล้ว": d['สำรวจแล้ว'],
        "ยังไม่สำรวจ":  d['ยังไม่สำรวจ']
      };
    }).toList();
  }

  @override
  _ExploreVisualizationState createState() => _ExploreVisualizationState();
}

class _ExploreVisualizationState extends State<ExploreVisualization> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header(widget._title),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 1,
                    child: StackedHorizontalBarChart(widget._mock),
                  ),
                  Positioned(
                    right: 10,
                    top: 0,
                    child: Row(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              width: 10,
                              height: 10,
                              color: BaseColor.MIDDLE_PURPLE,
                            ),
                            Text("สำรวจแล้ว")
                          ],
                        ),
                        SizedBox(width: 20,),
                        Column(
                          children: <Widget>[
                            Container(
                              width: 10,
                              height: 10,
                              color: BaseColor.PASTEL_VIOLET,
                            ),
                            Text("ยังไม่สำรวจ")
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: 20,),
              TableWidget(widget._data)
            ],
          ),
        ),
      ),
    );
  }
}
