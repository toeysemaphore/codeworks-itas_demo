import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/reporting.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/bar.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

String _d = "กระทรวง";
List<ReportingOitModel> mock = [
  new ReportingOitModel(
      department: "กลาโหม", sent: 2, withoutSave: 1, saved: 3),
  new ReportingOitModel(
      department: "ศึกษาธิการ", sent: 4, withoutSave: 2, saved: 2),
  new ReportingOitModel(
      department: "การคลัง", sent: 6, withoutSave: 3, saved: 3),
  new ReportingOitModel(
      department: "การต่างประเทศ", sent: 8, withoutSave: 4, saved: 4),
  new ReportingOitModel(
    department: "ท่องเที่ยว",
    sent: 10,
    withoutSave: 5,
    saved: 5,
  ),
  new ReportingOitModel(department: "สังคม", sent: 2, withoutSave: 1, saved: 3),
  new ReportingOitModel(
      department: "สหกรณ์", sent: 12, withoutSave: 6, saved: 6),
  new ReportingOitModel(
      department: "คมนาคม", sent: 18, withoutSave: 9, saved: 9),
  new ReportingOitModel(
      department: "ธรรมชาติ", sent: 20, withoutSave: 10, saved: 3),
  new ReportingOitModel(
      department: "มหาดไทย", sent: 14, withoutSave: 12, saved: 3),
  new ReportingOitModel(
      department: "สาธารณสุข", sent: 23, withoutSave: 12, saved: 6),
];

class ReportOitSubpage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ReportOitSubpageState();
  }
}

class ReportOitSubpageState extends State<ReportOitSubpage> {
  bool swap = false;
  bool show = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("รายงานการส่งแบบสำรวจ OIT"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              DropDownWidget("ปีงบประมาณ", YEARS_BUDGET()),
              SizedBox(
                height: 10,
              ),
              DropDownWidget("สังกัด", AFFILLATIONS()),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                color: BaseColor.MIDDLE_PURPLE,
                onPressed: () {
                  setState(() {
                    this.show = true;
                  });
                },
                child: Text(
                  "แสดง",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              !this.show
                  ? Container()
                  : Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            RotatedBox(
                              quarterTurns: this.swap ? 1 : 0,
                              child: Container(
                                height: 400,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Container(
                                    height: 400,
                                    width: 1000,
                                    child: StackedHorizontalBarChart<
                                            ReportingOitModel>(
                                        mock,
                                        ["sent", "withoutSave", "saved"],
                                        "department",
                                        "กราฟรายงานสรุปผลงบประมาณปี 2561 (OIT)"),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              right: 0,
                              child: IconButton(
                                icon: Icon(
                                  Icons.swap_calls,
                                  size: 30,
                                  color: Colors.red,
                                ),
                                autofocus: true,
                                splashColor: Colors.pink,
                                tooltip: "เปลี่ยนแนวกราฟ",
                                onPressed: () => setState(() {
                                  this.swap = !this.swap;
                                }),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TableWidget(
                            mock.map((m) => m.toJsonTH()).toList(), true)
                      ],
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
