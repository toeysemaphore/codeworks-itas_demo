import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/reporting.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/bar.dart';
import 'package:codework_itas/widgets/dropdown.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';

String _d = "กระทรวง";
List<ReportingIitModel> mock = [
  new ReportingIitModel(department: "กลาโหม", explored: 1, unExplore: 1),
  new ReportingIitModel(department: "ศึกษาธิการ", explored: 2, unExplore: 2),
  new ReportingIitModel(department: "การคลัง", explored: 3, unExplore: 1),
  new ReportingIitModel(
      department: "การต่างประเทศ", explored: 12, unExplore: 5),
  new ReportingIitModel(department: "ท่องเที่ยว", explored: 15, unExplore: 9),
  new ReportingIitModel(department: "สหกรณ์", explored: 6, unExplore: 6),
];

class ReportEitSubpage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ReportEitSubpageState();
  }
}

class ReportEitSubpageState extends State<ReportEitSubpage> {
  bool swap = false;
  bool show = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("รายงานการส่งแบบสำรวจ EIT"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              DropDownWidget("ปีงบประมาณ", YEARS_BUDGET()),
              SizedBox(
                height: 10,
              ),
              DropDownWidget("สังกัด", AFFILLATIONS()),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                color: BaseColor.MIDDLE_PURPLE,
                onPressed: () {
                  setState(() {
                    this.show = true;
                  });
                },
                child: Text(
                  "แสดง",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              !this.show
                  ? Container()
                  : Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            RotatedBox(
                              quarterTurns: this.swap ? 1 : 0,
                              child: Container(
                                height: 400,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Container(
                                    height: 400,
                                    width: 1000,
                                    child: StackedHorizontalBarChart<
                                            ReportingIitModel>(
                                        mock,
                                        ["explored", "unExplore"],
                                        "department",
                                        "กราฟรายงานสรุปผลงบประมาณปี 2561 (EIT)"),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              right: 0,
                              child: IconButton(
                                icon: Icon(
                                  Icons.swap_calls,
                                  size: 30,
                                  color: Colors.red,
                                ),
                                autofocus: true,
                                splashColor: Colors.pink,
                                tooltip: "เปลี่ยนแนวกราฟ",
                                onPressed: () => setState(() {
                                  this.swap = !this.swap;
                                }),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TableWidget(
                            mock.map((m) => m.toJsonTH()).toList(), true)
                      ],
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
