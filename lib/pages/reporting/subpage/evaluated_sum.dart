import 'dart:async';

import 'package:codework_itas/models/evaluate.dart';
import 'package:codework_itas/pages/common/explore_visualization/widgets/table.dart';
import 'package:codework_itas/widgets/indicator.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

final sample = [
  new EvaluateModel("สูงมาก", 122, 20, Colors.red).toJsonTH(),
  new EvaluateModel("สูง", 322, 40, Colors.red[300]).toJsonTH(),
  new EvaluateModel("ปานกลาง", 122, 10, Colors.yellow).toJsonTH(),
  new EvaluateModel("ต่ำ", 122, 20, Colors.blue).toJsonTH(),
  new EvaluateModel("ต่ำมาก", 122, 10, Colors.blue[400]).toJsonTH(),
];
final sample2 = [
  new EvaluateModel("สูงมาก", 122, 20, Colors.red),
  new EvaluateModel("สูง", 322, 40, Colors.red[300]),
  new EvaluateModel("ปานกลาง", 122, 10, Colors.yellow),
  new EvaluateModel("ต่ำ", 122, 20, Colors.blue),
  new EvaluateModel("ต่ำมาก", 122, 10, Colors.blue[400]),
];

class ReportingResult extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Header("รายงานสรุปผลการประเมิน"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(child: PieChartSample2()),
            SizedBox(
              height: 10,
            ),
            TableWidget(
              sample,
              true,
            )
          ],
        ),
      ),
    );
  }
}

class PieChartSample2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PieChart2State();
}

class PieChart2State extends State {
  List<PieChartSectionData> pieChartRawSections;
  List<PieChartSectionData> showingSections;

  StreamController<PieTouchResponse> pieTouchedResultStreamController;

  int touchedIndex;

  @override
  void initState() {
    super.initState();

    final items = sample2
        .map((s) => PieChartSectionData(
              color: s.color,
              value: s.percentage,
              title: s.level,
              radius: 140,
              titleStyle: TextStyle(
                fontSize: 18,
                fontFamily: "prompt",
                color: Color(0xffffffff),
              ),
            ))
        .toList();

    pieChartRawSections = items;

    showingSections = pieChartRawSections;

    pieTouchedResultStreamController = StreamController();
    pieTouchedResultStreamController.stream.distinct().listen((details) {
      if (details == null) {
        return;
      }

      touchedIndex = -1;
      if (details.sectionData != null) {
        touchedIndex = showingSections.indexOf(details.sectionData);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      child: AspectRatio(
        aspectRatio: 1,
        child: FlChart(
          chart: PieChart(
            PieChartData(
              pieTouchData: PieTouchData(
                  touchResponseStreamSink:
                      pieTouchedResultStreamController.sink),
              borderData: FlBorderData(
                show: false,
              ),
              sectionsSpace: 0,
              centerSpaceRadius: 0,
              sections: showingSections,
            ),
          ),
        ),
      ),
    );
  }
}
