import 'package:codework_itas/const.dart';
import 'package:codework_itas/utils/memory.dart';
import 'package:flutter/material.dart';

class ButtonLoginWidget extends StatelessWidget {
  Function login;

  ButtonLoginWidget(this.login);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: login,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(20),
        topRight: Radius.circular(20),
        bottomRight: Radius.circular(20),
        topLeft: Radius.circular(20),
      )),
      color: BaseColor.MIDDLE_PURPLE,
      child: Container(
        child: Center(
            child: Text(
          "เข้าสู่ระบบ",
          style: Theme.of(context).textTheme.button,
        )),
        width: 200,
        height: 60,
      ),
    );
  }
}
