import 'package:flutter/material.dart';

class FormLoginWidget extends StatefulWidget {
  final GlobalKey<FormState> _formkey;
  final Function handleUsername;
  final Function handlePassword;
  FormLoginWidget(this._formkey, this.handlePassword, this.handleUsername);

  @override
  _FormLoginWidgetState createState() => _FormLoginWidgetState();
}

class _FormLoginWidgetState extends State<FormLoginWidget> {
  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 50),
      child: Form(
        key: widget._formkey,
        child: Column(
          children: <Widget>[
            TextFormField(
              onSaved: widget.handleUsername,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  border: InputBorder.none,
                  filled: true,
                  labelText: "username",
                  icon: Icon(Icons.people)),
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              onSaved: widget.handlePassword,
              obscureText: true,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  border: InputBorder.none,
                  filled: true,
                  labelText: "password",
                  icon: Icon(Icons.no_encryption)),
            ),
          ],
        ),
      ),
    );
  }
}
