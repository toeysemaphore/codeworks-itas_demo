import 'dart:async';
import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/notification.dart';
import 'package:codework_itas/widgets/appbar.dart';
import 'package:codework_itas/widgets/title_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationPage extends StatefulWidget {
  @override
  NotificationPageState createState() => NotificationPageState();
}

class NotificationPageState extends State<NotificationPage> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  List<NotificationModel> _mock = [
    new NotificationModel("คณะกรรมการ ป.ป.ช มีมติ ให้ น.ส. มนัญญา ไทยเศรษฐ์",
        "น.ส. มนัญญา ไทยเศรษฐ์ รมช.เกษตรฯ ต้องยื่นบัญชีทรัพย์สิน ทั้ง 2 กรณี เมื่อพ้นจากตำแหน่งนายกเทศมนตรีเมืองอุทัยธานีและกรณีเข้ารับตำแหน่ง รมช.เกษตรฯ"),
    new NotificationModel(
        "ป.ป.ช. จัดสัมมนาคณะอนุกรรมการขับเคลื่อนการส่งเสริมและสนับสนุนให้ประชาชนและหน่วยงานของรัฐ",
        "สัมมนาคณะอนุกรรมการขับเคลื่อนการส่งเสริมและสนับสนุนให้ประชาชนและหน่วยงานของรัฐ มีส่วนร่วมในการป้องกันและปราบปรามการทุจริต"),
    new NotificationModel("รายชื่อบุคคลที่ถูกกำหนดตามมาตรา 6 แห่ง",
        "รายชื่อบุคคลที่ถูกกำหนดตามมาตรา 6 แห่ง พ.ร.บ.ป้องกันและปราบปรามการสนับสนุนทางการเงินแก่การก่อการร้ายและการแพร่ขยายอาวุธที่มีอานุภาพทำลายล้างสูง พ.ศ. 2559 (กลุ่ม Al-Qaida) ประกาศสำนักงาน ปปง. ที่ 15/2562"),
  ];
  @override
  void initState() {
    super.initState();
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('logo');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);

    Future.delayed(Duration(seconds: 1)).then((_) {
      this.showNotification();
    });
  }

  Future onSelectNotification(String payload) {
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        elevation: 10,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        title: Row(
          children: <Widget>[
            new Text(
              'คุณได้รับการแจ้งเตือนใหม่',
              style: TextStyle(color: Colors.black,fontSize: 16),
            ),
            Icon(
              Icons.notifications_active,
              color: BaseColor.MIDDLE_PURPLE,
            )
          ],
        ),
        content: new Text('$payload'),
        actions: <Widget>[
          RaisedButton(
            color: BaseColor.IMPERIAL,
            child: Text(
              "รับทราบ",
              style: TextStyle(color: Colors.white,fontSize: 17),
            ),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerME(),
      appBar: new AppBar(
        title: new Header('แจ้งเตือน'),
      ),
      body: ListView.separated(
        separatorBuilder: (_, i) => Padding(
          child: Divider(),
          padding: EdgeInsets.only(top: 2),
        ),
        itemCount: this._mock.length + 1,
        itemBuilder: (BuildContext context, int index) =>
            index == this._mock.length
                ? Container()
                : ListTile(
                    title: Text(this._mock[index].title),
                    subtitle: Text(this._mock[index].body),
                    isThreeLine: true,
                    trailing: this._mock[index].isNew
                        ? Container(
                            height: 30,
                            width: 30,
                            child: Image.asset(
                              "assets/imgs/alerts.jpg",
                              fit: BoxFit.cover,
                            ),
                          )
                        : SizedBox(
                            height: 0,
                            width: 0,
                          ),
                  ),
      ),
    );
  }

  showNotification() async {
    var android = new AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.High, importance: Importance.Max);
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android, iOS);
    setState(() {
      this._mock.insert(
          0,
          new NotificationModel("คุณได้รับการแจ้งเตือนใหม่",
              "รายชื่อบุคคลที่ถูกกำหนดตามมาตรา 6 แห่ง พ.ร.บ.ป้องกันและปราบปรามการสนับสนุนทางการเงินแก่การก่อการร้ายและการแพร่ขยายอาวุธที่มีอานุภาพทำลายล้างสูง พ.ศ. 2559 (กลุ่ม Al-Qaida) ประกาศสำนักงาน ปปง. ที่ 15/2562", true));
    });
    await flutterLocalNotificationsPlugin.show(0, 'คุณได้รับการแจ้งเตือนใหม่',
        'สามารถแตะเพื่อดูรายละเอียดเพิ่มเติม', platform,
        payload: "รายชื่อบุคคลที่ถูกกำหนดตามมาตรา 6 แห่ง พ.ร.บ.ป้องกันและปราบปรามการสนับสนุนทางการเงินแก่การก่อการร้ายและการแพร่ขยายอาวุธที่มีอานุภาพทำลายล้างสูง พ.ศ. 2559 (กลุ่ม Al-Qaida) ประกาศสำนักงาน ปปง. ที่ 15/2562");
  }
}
