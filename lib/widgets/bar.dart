import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/reporting.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class StackedHorizontalBarChart<T extends Reporting> extends StatelessWidget {
  final bool animate = false;
  List<T> _mock;
  List<String> _keys = [];
  String title = "";
  StackedHorizontalBarChart(this._mock, this._keys, this._type, this.title);
  String _type;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        new charts.BarChart(
          _createSampleData(),
          animate: animate,
          barGroupingType: charts.BarGroupingType.stacked,
          vertical: true,
        ),
        Positioned(
          left: MediaQuery.of(context).size.width * .1,
          top: 50,
          child: Center(
            child: Text(
              title,
              style: TextStyle(
                  color: BaseColor.IMPERIAL, fontSize: 15, letterSpacing: 1),
            ),
          ),
        )
      ],
    );
  }

  List<charts.Series<T, String>> _createSampleData() {
    return this._keys.map((key) {
      return new charts.Series<T, String>(
          id: key,
          domainFn: (T d, _) => d.getDataByKey(this._type),
          measureFn: (T d, _) => int.parse(d.getDataByKey(key)),
          data: this._mock);
    }).toList();
  }
}
