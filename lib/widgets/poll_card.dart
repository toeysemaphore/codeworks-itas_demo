import 'package:codework_itas/const.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart' as gb;

class PollCardWidget extends StatefulWidget {
  List<dynamic> asks;
  String title;
  String type;

  PollCardWidget(
      {@required this.asks, @required this.title, @required this.type});

  @override
  _PollCardWidgetState createState() => _PollCardWidgetState();
}

class _PollCardWidgetState extends State<PollCardWidget> {
  bool _isRadio;

  @override
  void initState() {
    super.initState();
    this._isRadio = this.checkTypeIsRadio();
  }

  bool checkTypeIsRadio() {
    if (widget.type == TYPES[0]) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          width: 5,
          color: Colors.black,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(5),
            color: Colors.grey[300],
            child: Text(
              widget.title,
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .copyWith(color: Colors.black),
            ),
          ),
          this._isRadio
              ? gb.RadioButtonGroup(
                  labelStyle: Theme.of(context)
                      .textTheme
                      .display1
                      .copyWith(color: Colors.black),
                  labels: widget.asks.map((d) {
                    return d.toString();
                  }).toList(),
                )
              : gb.CheckboxGroup(
                  labelStyle: Theme.of(context)
                      .textTheme
                      .display1
                      .copyWith(color: Colors.black),
                  labels: widget.asks.map((d) {
                    return d.toString();
                  }).toList())
        ],
      ),
    );
  }
}
