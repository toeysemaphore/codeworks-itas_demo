import 'package:codework_itas/const.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  final String title;
  final List<String> data;

  DropDownWidget(this.title, this.data);
  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String _selected = "";
  bool isInsert = !false;

  void _onSelected(int index) {
    setState(() {
      this._selected = widget.data[index];
    });
  }

  @override
  void initState() {
    super.initState();
    if (this.isInsert) {
      widget.data.insert(0, widget.title);
      this.isInsert = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      color: BaseColor.IMPERIAL,
      child: Container(
        // width: 150,
        child: Row(
          children: <Widget>[
            Flexible(
                child: Container(
              padding: EdgeInsets.all(8),
              child: Center(
                child: Text(
                  this._selected == "" ? widget.title : this._selected,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle
                      .copyWith(color: Colors.white, fontSize: 16),
                ),
              ),
            )),
            Container(
              height: 60,
              width: 60,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                color: Colors.white.withOpacity(.4),
                child: Center(
                    child: Icon(
                  Icons.arrow_drop_down,
                  size: 50,
                  color: BaseColor.IMPERIAL,
                )),
                onPressed: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (BuildContext context) => Container(
                      height: 300,
                      child: CupertinoPicker.builder(
                        onSelectedItemChanged: this._onSelected,
                        itemExtent: 30,
                        childCount: widget.data.length,
                        itemBuilder: (BuildContext context, int index) =>
                            GestureDetector(
                          child: Text(widget.data[index]),
                          onTap: () => this._onSelected(index),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
