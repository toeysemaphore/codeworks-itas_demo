import 'package:codework_itas/const.dart';
import 'package:codework_itas/models/user.dart';
import 'package:codework_itas/utils/user.dart';
import 'package:flutter/material.dart';

class DrawerME extends StatelessWidget {
  UserModel user = User.user;
  @override
  Drawer build(BuildContext context) {
    return Drawer(
      elevation: 20,
      child: Container(
        color: Colors.grey[400].withOpacity(.4),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                    color: BaseColor.MIDDLE_PURPLE.withOpacity(.8)),
                accountEmail: Text(user.email),
                accountName: Text(user.fname + "  " + user.lname),
                arrowColor: Colors.white,
                currentAccountPicture: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  backgroundImage: AssetImage("assets/imgs/avatar.jpg"),
                ),
                onDetailsPressed: () {
                  Navigator.of(context).pushNamed("/me");
                },
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("หน้าหลัก"),
                selected: ModalRoute.of(context).settings.name == "/main",
                onTap: () => Navigator.of(context)
                    .pushNamedAndRemoveUntil("/main", (route) {
                  return !true;
                }),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("แบบสำรวจ OIT"),
                selected: ModalRoute.of(context).settings.name == "/oit",
                onTap: () => Navigator.of(context).pushNamed("/oit"),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("แบบสำรวจ IIT"),
                selected: ModalRoute.of(context).settings.name == "/iit",
                onTap: () => Navigator.of(context).pushNamed("/iit"),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("แบบสำรวจ EIT"),
                onTap: () => Navigator.of(context).pushNamed("/eit"),
                selected: ModalRoute.of(context).settings.name == "/eit",
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("ติดตามแบบสำรวจ"),
                onTap: () => Navigator.of(context).pushNamed("/followup"),
                selected: ModalRoute.of(context).settings.name == "/followup",
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: ExpansionTile(
                  title: Text("รายงาน"),
                  children: <Widget>[
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("การส่งแบบสำรวจ OIT"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/reporting/oit"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/oit",
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("การส่งแบบสำรวจ IIT"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/reporting/iit"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/iit",
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("การส่งแบบสำรวจ EIT"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/reporting/eit"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/eit",
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("สรุปผลการประเมิน"),
                      onTap: () => Navigator.of(context)
                          .pushNamed("/reporting/evaluate"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/evaluate",
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("สรุปผลการประเมินช่วงปีงบประมาณ"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/reporting/summary"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/summary",
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 20),
                      title: Text("ประวัติการใช้งาน"),
                      onTap: () =>
                          Navigator.of(context).pushNamed("/reporting/history"),
                      selected: ModalRoute.of(context).settings.name ==
                          "/reporting/history",
                    ),
                  ],
                ),
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("Poll"),
                onTap: () => Navigator.of(context).pushNamed("/poll"),
                selected: ModalRoute.of(context).settings.name == "/poll",
              ),
              ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 20),
                title: Text("แจ้งเตือน"),
                onTap: () => Navigator.of(context).pushNamed("/notification"),
                selected:
                    ModalRoute.of(context).settings.name == "/notification",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
