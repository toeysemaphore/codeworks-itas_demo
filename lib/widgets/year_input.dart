import 'package:codework_itas/const.dart';
import 'package:flutter/material.dart';

class YearInput extends StatefulWidget {
  @override
  _YearInputState createState() => _YearInputState();
}

class _YearInputState extends State<YearInput> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      child: TextField(
        decoration: InputDecoration(
          labelText: "ปีงบประมาณ",
          contentPadding: EdgeInsets.all(20),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: BaseColor.BUMBLE_GUM)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide: BorderSide(
              color: BaseColor.RUDDY_PINK,
              width: 10,
            ),
          ),
          fillColor: Colors.white,
          filled: true,
        ),
      ),
    );
  }
}
